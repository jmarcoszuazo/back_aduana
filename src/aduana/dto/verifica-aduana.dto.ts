import { IsNumber, IsPositive, IsDate, IsString } from 'class-validator';
import { Timestamp } from 'typeorm';

export class VerificaAduanaDto {
  @IsNumber()
  @IsPositive()
  id: number;

  @IsString()
  fecha: string;

  @IsString()
  doc: string;
}
