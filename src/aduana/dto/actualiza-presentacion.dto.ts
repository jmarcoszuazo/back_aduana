import { IsNumber, IsPositive, IsDate, IsString } from 'class-validator';

export class ActualizaPresentacionDto {
  @IsString()
  idForm: string;

  @IsString()
  presentacion: string;
}
