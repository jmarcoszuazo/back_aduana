import { Controller, Get } from '@nestjs/common';
import { OperadorService } from '../services/operador.service';

@Controller('operador')
export class OperadorController {
  constructor(private readonly operadorService: OperadorService) {}

  @Get()
  findAll() {
    console.log('traer operadores');
    return this.operadorService.findAll();
  }
}
