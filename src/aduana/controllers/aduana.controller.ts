import {
  Body,
  Controller,
  Delete,
  Get,
  Query,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';

import { AduanaService } from '../services/aduana.service';
import { VerificaAduanaDto } from '../dto/verifica-aduana.dto';
import { ActualizaPresentacionDto } from '../dto/actualiza-presentacion.dto';

@Controller('aduana')
export class AduanaController {
  constructor(private readonly aduanaService: AduanaService) {}

  //findOne
  @Get()
  findAll() {
    console.log('traer los usuarioss');
    return this.aduanaService.findAll();
  }

  @Get('uno')
  async findOne(@Query() verificaAduanaDto: VerificaAduanaDto) {
    //console.log('entra-veri al controldor');
    return this.aduanaService.findOne(verificaAduanaDto);
  }

  @Post()
  actualiza(@Body() actualizaPresentacionDto: ActualizaPresentacionDto) {
    return this.aduanaService.actualizaPre(actualizaPresentacionDto);
  }
}
