import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Formm03CalculoRm } from './formm03-calculo-rm.entity';
import { Operador } from './operador.entity';
import { Formm03 } from './formm_03.entity';

@Entity({ name: 'aduana' })
export class AduanaSalida {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'codigo',
    type: 'varchar',
  })
  codigo: string;

  @Column({
    name: 'descripcion',
    type: 'varchar',
  })
  descripcion: string;

  @OneToOne(() => Formm03, (formm03) => formm03.aduanaSalida)
  formm03: Formm03;
}
