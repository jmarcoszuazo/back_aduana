import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Formm03CalculoRm } from './formm03-calculo-rm.entity';
import { Operador } from './operador.entity';
import { AduanaSalida } from './aduana-salida.entity';

@Entity({ name: 'formm03' })
export class Formm03 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'codigoformm03',
    type: 'varchar',
  })
  codigoFormm03: string;

  @Column({
    name: 'idexportador',
    type: 'int',
  })
  idExportador: number;

  @Column({
    name: 'idiso3166pais',
    type: 'int',
  })
  idIso3166Pais: number;

  @Column({
    name: 'razonsocialcomprador',
    type: 'varchar',
  })
  razonSocialComprador: string;

  @Column({
    name: 'consignatario',
    type: 'varchar',
  })
  consignatario: string;

  @Column({
    name: 'fechatransaccion',
    type: 'date',
  })
  fechaTransaccion: Date;

  @Column({
    name: 'fechaexportacion',
    type: 'date',
  })
  fechaExportacion: Date;

  @Column({
    name: 'fechadeclaracion',
    type: 'date',
  })
  fechaDeclaracion: Date;

  @Column({
    name: 'fechavalidacion',
    type: 'date',
  })
  fechaValidacion: Date;

  @Column({
    name: 'tipocambiobs',
    type: 'numeric',
  })
  tipoCambioBs: number;

  @Column({
    name: 'nrofactura',
    type: 'int',
  })
  nroFactura: number;

  @Column({
    name: 'lote',
    type: 'varchar',
  })
  lote: string;

  @Column({
    name: 'idpresentacionproducto',
    type: 'int',
  })
  idPresentacionProducto: number;

  @Column({
    name: 'idlaboratorio',
    type: 'int',
  })
  idLaboratorio: number;

  @Column({
    name: 'certificadoanalisis',
    type: 'varchar',
  })
  certificadoAnalisis: string;

  @Column({
    name: 'idaduana',
    type: 'int',
  })
  idAduana: number;

  @Column({
    name: 'due',
    type: 'varchar',
  })
  due: string;

  @Column({
    name: 'pbh',
    type: 'numeric',
  })
  pbh: number;

  @Column({
    name: 'tara',
    type: 'numeric',
  })
  tara: number;

  @Column({
    name: 'pnh',
    type: 'numeric',
  })
  pnh: number;

  @Column({
    name: 'humedad',
    type: 'numeric',
  })
  humedad: number;

  @Column({
    name: 'mermaporcentaje',
    type: 'numeric',
  })
  mermaPorcentaje: number;

  @Column({
    name: 'mermakg',
    type: 'numeric',
  })
  mermaKg: number;

  @Column({
    name: 'pns',
    type: 'numeric',
  })
  pns: number;

  @Column({
    name: 'totalkilosfinos',
    type: 'numeric',
  })
  totalKilosFinos: number;

  @Column({
    name: 'totalvbvusd',
    type: 'numeric',
  })
  totalVbvUsd: number;

  @Column({
    name: 'totalvbvbs',
    type: 'numeric',
  })
  totalVbvBs: number;

  @Column({
    name: 'totalrmusd',
    type: 'numeric',
  })
  totalRmUsd: number;

  @Column({
    name: 'totalrmbs',
    type: 'numeric',
  })
  totalRmBs: number;

  @Column({
    name: 'gastosacordado',
    type: 'numeric',
  })
  gastosAcordado: number;

  @Column({
    name: 'vnv',
    type: 'numeric',
  })
  vnv: number;

  @Column({
    name: 'totaldeducciones',
    type: 'numeric',
  })
  totaldeducciones: number;

  @Column({
    name: 'liquido',
    type: 'numeric',
  })
  liquido: number;

  @Column({
    name: 'totaldeducciones',
    type: 'numeric',
  })
  totalImporteRmBs: number;

  @Column({
    name: 'totaldepartamentormbs',
    type: 'numeric',
  })
  totaldepartamentormbs: number;

  @Column({
    name: 'totalmunicipiormbs',
    type: 'numeric',
  })
  totalMunicipioRmBs: number;

  @Column({
    name: 'totalaporteentidadbs',
    type: 'numeric',
  })
  totalAporteEntidadBs: number;

  @Column({
    name: 'oficinavalidacion',
    type: 'varchar',
  })
  oficinaValidacion: string;

  @Column({
    name: 'codigovalidador',
    type: 'varchar',
  })
  codigoValidador: string;

  @Column({
    name: 'codigooperador',
    type: 'varchar',
  })
  codigoOperador: string;

  @Column({
    name: 'representanteexportador',
    type: 'varchar',
  })
  representanteExportador: string;

  @Column({
    name: 'nrodocumento',
    type: 'varchar',
  })
  nroDocumento: string;

  @Column({
    name: 'expedicion',
    type: 'varchar',
  })
  expedicion: string;

  @Column({
    name: 'estado',
    type: 'int',
  })
  estado: number;

  @Column({
    name: 'bandera',
    type: 'boolean',
  })
  bandera: boolean;

  @Column({
    name: 'fecharegistro',
    type: 'date',
  })
  fecharegistro: Date;

  @Column({
    name: 'observaciones',
    type: 'varchar',
  })
  observaciones: string;

  @Column({
    name: 'idlugarnim',
    type: 'int',
  })
  idLugarNim: number;

  @Column({
    name: 'fob',
    type: 'numeric',
  })
  fob: number;

  @Column({
    name: 'incoterm',
    type: 'varchar',
  })
  incoterm: string;

  @Column({
    name: 'idrectificacion',
    type: 'int',
  })
  idRectificacion: number;

  @Column({
    name: 'aportecns_recalculo',
    type: 'numeric',
  })
  aporteCnsRecalculo: number;

  @Column({
    name: 'aportesnrcm_recalculo',
    type: 'numeric',
  })
  aporteSnrcmRecalculo: number;

  @Column({
    name: 'fecha_recalculo',
    type: 'date',
  })
  fechaRecalculo: Date;

  @Column({
    name: 'id_recalculo',
    type: 'int',
  })
  idRecalculo: number;

  @Column({
    name: 'totalrm_recalculo',
    type: 'numeric',
  })
  totalRmRecalculo: number;

  @Column({
    name: 'responsablerectificacion',
    type: 'varchar',
  })
  responsableRectificacion: string;

  @Column({
    name: 'responsableanulacion',
    type: 'varchar',
  })
  responsableAnulacion: string;

  @Column({
    name: 'estadorevision',
    type: 'int',
  })
  estadoRevision: number;

  @Column({
    name: 'direccionarchivodigital',
    type: 'varchar',
  })
  direccionArchivoDigital: string;

  @Column({
    name: 'banderaarchivodigital',
    type: 'int',
  })
  banderaArchivoDigital: number;

  @Column({
    name: 'fechaenviomuestra',
    type: 'date',
  })
  fechaEnvioMuestra: Date;

  @Column({
    name: 'codigoenviomuestra',
    type: 'varchar',
  })
  codigoEnvioMuestra: string;

  @Column({
    name: 'citeenviomuestra',
    type: 'varchar',
  })
  citeEnvioMuestra: string;

  @Column({
    name: 'fechamuestra',
    type: 'date',
  })
  fechaMuestra: Date;

  @Column({
    name: 'codigomuestra',
    type: 'varchar',
  })
  codigoMuestra: string;

  @Column({
    name: 'presentacion',
    type: 'varchar',
  })
  presentacion: string;

  @OneToMany(
    () => Formm03CalculoRm,
    (formm03CalculoRm) => formm03CalculoRm.formm03,
    { eager: true },
  )
  //@JoinColumn({ name: 'idformm03', referencedColumnName: 'id' })
  formm03CalculoRm: Formm03CalculoRm;

  @OneToOne(() => Operador, (operador) => operador.formm03, { eager: true })
  @JoinColumn({ name: 'idexportador', referencedColumnName: 'id' })
  operador: Operador;

  @OneToOne(() => AduanaSalida, (aduanaSalida) => aduanaSalida.formm03, {
    eager: true,
  })
  @JoinColumn({ name: 'idaduana', referencedColumnName: 'id' })
  aduanaSalida: AduanaSalida;
}
