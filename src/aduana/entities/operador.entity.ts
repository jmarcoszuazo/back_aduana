import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Formm03 } from './formm_03.entity';

@Entity({ name: 'operador' })
export class Operador {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'idtipopersona',
    type: 'int',
  })
  idtipoPersona: number;

  @Column({
    name: 'nombre',
    type: 'varchar',
  })
  nombre: string;

  @Column({
    name: 'documento',
    type: 'varchar',
  })
  documento: string;

  @Column({
    name: 'idsubsector',
    type: 'int',
  })
  idSubSector: number;

  @Column({
    name: 'ruex',
    type: 'varchar',
  })
  ruex: string;

  @Column({
    name: 'nua',
    type: 'varchar',
  })
  nua: string;

  @Column({
    name: 'cns',
    type: 'varchar',
  })
  cns: string;

  @Column({
    name: 'fundempresa',
    type: 'varchar',
  })
  fundEmpresa: string;

  @Column({
    name: 'casamatriz',
    type: 'varchar',
  })
  casaMatriz: string;

  @Column({
    name: 'idmunicipio',
    type: 'int',
  })
  idMunicipio: number;

  @Column({
    name: 'direccion',
    type: 'varchar',
  })
  direccion: string;

  @Column({
    name: 'sitioweb',
    type: 'varchar',
  })
  sitioWeb: string;

  @Column({
    name: 'correoelectronico',
    type: 'varchar',
  })
  correoElectronico: string;

  @Column({
    name: 'telefono',
    type: 'varchar',
  })
  telefono: string;

  @Column({
    name: 'fax',
    type: 'varchar',
  })
  fax: string;

  @Column({
    name: 'nim',
    type: 'varchar',
  })
  nim: string;

  @Column({
    name: 'idlugarnim',
    type: 'int',
  })
  idLugarNim: number;

  @Column({
    name: 'fechanim',
    type: 'timestamp',
  })
  fechaNim: Date;

  @Column({
    name: 'fechaexpiracion',
    type: 'timestamp',
  })
  fechaExpiracion: Date;

  @Column({
    name: 'codigousuario',
    type: 'varchar',
  })
  codigoUsuario: string;

  @Column({
    name: 'estado',
    type: 'int',
  })
  estado: number;

  @Column({
    name: 'fecharenovacion',
    type: 'timestamp',
  })
  fechaRenovacion: Date;

  @Column({
    name: 'nrorenovacionnim',
    type: 'int',
  })
  nroRenovacioNim: number;
  @Column({
    name: 'resolucion',
    type: 'varchar',
  })
  resolucion: string;

  @Column({
    name: 'm03',
    type: 'int',
  })
  m03: number;

  @Column({
    name: 'observacion',
    type: 'varchar',
  })
  observacion: string;

  @Column({
    name: 'codigobarra',
    type: 'varchar',
  })
  codigoBarra: string;

  @Column({
    name: 'minerales',
    type: 'varchar',
  })
  minerales: string;

  @Column({
    name: 'actividad_agente',
    type: 'varchar',
  })
  actividadAgente: string;

  @Column({
    name: 'tipo_actor',
    type: 'varchar',
  })
  tipoActor: string;

  @Column({
    name: 'resfinicio',
    type: 'timestamp',
  })
  resfInicio: Date;

  @Column({
    name: 'comentarios_bd',
    type: 'varchar',
  })
  comentariosBd: string;

  @Column({
    name: 'fecha_migracion_niar',
    type: 'timestamp',
  })
  fechaMigracionNiar: Date;

  @Column({
    name: 'operador_economico_autorizado',
    type: 'varchar',
  })
  operadorEconomicoAutorizado: string;

  @Column({
    name: 'operador_economico_autorizado_fechainicio',
    type: 'timestamp',
  })
  operadorEconomicoAutorizadoFechaInicio: Date;

  @Column({
    name: 'operador_economico_autorizado_fechafin',
    type: 'timestamp',
  })
  operadorEconomicoAutorizadoFechaFin: Date;

  @Column({
    name: 'vigente',
    type: 'varchar',
  })
  vigente: string;

  @OneToOne(() => Formm03, (formm03) => formm03.operador)
  formm03: Formm03;
}
