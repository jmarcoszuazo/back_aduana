import { Test, TestingModule } from '@nestjs/testing';
import { AduanaService } from './aduana.service';

describe('AduanaService', () => {
  let service: AduanaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AduanaService],
    }).compile();

    service = module.get<AduanaService>(AduanaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
