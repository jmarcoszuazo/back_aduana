import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository, SelectQueryBuilder } from 'typeorm';

import { Formm03 } from '../entities/formm_03.entity';
import { VerificaAduanaDto } from '../dto/verifica-aduana.dto';
import { ActualizaPresentacionDto } from '../dto/actualiza-presentacion.dto';

@Injectable()
export class AduanaService {
  constructor(
    @InjectRepository(Formm03)
    private readonly formm03Repository: Repository<Formm03>,
  ) {}

  findAll() {
    return this.formm03Repository.find({
      where: {
        id: 72620,
      },
    });
  }

  async buscarUno(id: number) {
    console.log('ingresa a funcion buscarUno');
    const fr = await this.formm03Repository.findOneBy({ id });
    return fr;
  }

  async findOne(verificaAduanaDto: VerificaAduanaDto) {
    if (!(await this.validarFecha(verificaAduanaDto))) {
      throw new InternalServerErrorException('LA FECHA ES ANTERIOR A ENERO');
    } else {
      console.log(
        typeof verificaAduanaDto.fecha + `: ${verificaAduanaDto.fecha} query`,
      );

      const result = await this.formm03Repository
        .createQueryBuilder('formm03')
        .leftJoinAndSelect('formm03.operador', 'operador')
        .leftJoinAndSelect('formm03.formm03CalculoRm', 'formm03CalculoRm')
        .leftJoinAndSelect('formm03.aduanaSalida', 'aduanaSalida')
        .where('formm03.id = :id', { id: verificaAduanaDto.id })
        //.andWhere('formm03.estado = :estado', { estado: 2 })
        .andWhere('formm03.fechavalidacion = :fechavalidacion', {
          fechavalidacion: verificaAduanaDto.fecha,
        })
        .andWhere('operador.documento = :documento', {
          documento: verificaAduanaDto.doc,
        })
        .getOne();
      if (!result) throw new InternalServerErrorException('Error interno');
      //console.log(result);
      return result;
    }
  }

  async validarFecha(verificaAduanaDto: VerificaAduanaDto) {
    // Obtener la fecha actual
    const fechaActual = new Date();

    // Restar 4 meses a la fecha actual
    const fechaLimite = new Date(2023, 0, 1);
    //   fechaActual.getFullYear(),
    //   fechaActual.getMonth() - 4,
    //   fechaActual.getDate(),
    // );

    // Convertir las fechas a milisegundos para comparar
    const fechaIngresadaMs = await new Date(verificaAduanaDto.fecha).getTime();
    const fechaLimiteMs = await fechaLimite.getTime();

    // Comparar la fecha ingresada con la fecha límite
    if (fechaIngresadaMs >= fechaLimiteMs) {
      // La fecha ingresada no pasa los 4 meses hacia atrás
      console.log('la fecha esta entre los enero: verdad');
      return true;
    } else {
      // La fecha ingresada es anterior a los 4 meses hacia atrás
      console.log('la fecha es anterior a enero: false');
      return false;
    }
  }

  async actualizaPre(actualizaPresentacionDto: ActualizaPresentacionDto) {
    try {
      const form = await this.buscarUno(
        parseInt(actualizaPresentacionDto.idForm),
      );
      if (actualizaPresentacionDto.presentacion == 'SI') {
        const { presentacion, ...formPersiste } = form;
        const formUpdated = await this.formm03Repository.preload({
          id: parseInt(actualizaPresentacionDto.idForm),
          presentacion: actualizaPresentacionDto.presentacion,
          ...formPersiste,
        });
        await this.formm03Repository.save(formUpdated);
        const fr = await this.buscarUno(
          parseInt(actualizaPresentacionDto.idForm),
        );
        return fr;
      } else {
        return form;
      }
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException('Error interno');
    }
  }
}
