import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Formm03CalculoRm } from '../entities/formm03-calculo-rm.entity';

@Injectable()
export class Formm03CalculoRmService {
  constructor(
    @InjectRepository(Formm03CalculoRm)
    private readonly formm03CalculoRmRepository: Repository<Formm03CalculoRm>,
  ) {}

  async findAll() {
    return await this.formm03CalculoRmRepository.find();
  }
}
