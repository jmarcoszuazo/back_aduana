import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';

import * as md5 from 'md5';

import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { Usuario } from './entities/usuario.entity';
import { LoginUsuarioDto } from './dto/login-usuario.dto';
import { JwtPayload } from './interfaces/jwt-payload.interface';

@Injectable()
export class UsuarioService {
  constructor(
    @InjectRepository(Usuario)
    private readonly userRepository: Repository<Usuario>,

    private readonly jwtService: JwtService,
  ) {}

  create(createUsuarioDto: CreateUsuarioDto) {
    return 'This action adds a new usuario';
  }

  findAll() {
    return this.userRepository.find({});
  }

  findOne(id: number) {
    return `This action returns a #${id} usuario`;
  }

  update(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    return `This action updates a #${id} usuario`;
  }

  remove(id: number) {
    return `This action removes a #${id} usuario`;
  }

  async login(loginUsuarioDto: LoginUsuarioDto) {
    const { usuario, contrasena } = loginUsuarioDto;
    let mdContra = md5(contrasena).toUpperCase();
    console.log(mdContra);
    const user = await this.userRepository.findOne({
      where: { usuario: usuario, contrasena: mdContra },
      select: { usuario: true, contrasena: true },
    });
    if (!user) {
      throw new UnauthorizedException('Credenciales no validas');
    }
    return {
      ...user,
      token: this.getJwtToken({ usuario: usuario }),
    };
  }

  private getJwtToken(payload: JwtPayload) {
    const token = this.jwtService.sign(payload);
    return token;
  }
}
