import { IsString } from 'class-validator';

export class LoginUsuarioDto {
  @IsString({
    message: 'El atributo usuario debe ser una cadena y es obligatorio',
  })
  usuario: string;

  @IsString({
    message: 'El atributo contrasena es una cadena y debe ser obligatoro',
  })
  contrasena: string;
}
